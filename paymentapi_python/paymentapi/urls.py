from django.urls import path
from . import views

urlpatterns = [
    path('c2ctransfer', views.c2ctransfer),
    # path('transaction_details', views.transaction_details),
    # path('transaction_history', views.transaction_history),
]
