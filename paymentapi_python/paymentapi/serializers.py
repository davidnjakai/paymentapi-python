from rest_framework.serializers import ModelSerializer
from paymentapi_python.paymentapi import models


class ChargeTariffSerializer(ModelSerializer):
    class Meta:
        model = models.ChargeTariff
        fields = "__all__"

class CustomerDetailSerializer(ModelSerializer):
    class Meta:
        model = models.CustomerDetail
        fields = "__all__"

class MobileWalletSerializer(ModelSerializer):
    class Meta:
        model = models.MobileWallet
        fields = "__all__"

class BusinessAccountSerializer(ModelSerializer):
    class Meta:
        model = models.BusinessAccount
        fields = "__all__"

class MainTransactionSerializer(ModelSerializer):
    class Meta:
        model = models.MainTransaction
        fields = "__all__"

class SubTransactionSerializer(ModelSerializer):
    class Meta:
        model = models.SubTransaction
        fields = "__all__"
