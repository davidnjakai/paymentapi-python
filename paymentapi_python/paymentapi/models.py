from django.db import models

# Create your models here.

class CustomerDetail(models.Model):
    """This stores details about a customer in the XYZ network"""
    STATUSES = (('unregistered', 'Not registered'), ('registered', 'Registered'))

    surname = models.CharField(max_length=255)
    firstname = models.CharField(max_length=255)
    secondname = models.CharField(max_length=255)
    id_number = models.CharField(max_length=10)
    phone_number = models.IntegerField(unique=True)
    status = models.CharField(max_length=20, choices=STATUSES, default="registered")
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

class MobileWallet(models.Model):
    """docstring for MobileWallet."""
    phone_number = models.OneToOneField(
        CustomerDetail,
        on_delete=models.CASCADE,
        primary_key=True,
        to_field="phone_number",
        db_column="phone_number"
    )
    account_balance = models.DecimalField(max_digits=14, decimal_places=2)
    pin = models.CharField(max_length=100)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def has_enough_balance(self, amount, charge_tariff, to_registered=True):
        if to_registered:
            charge = charge_tariff.send_to_registered
        else:
            charge = charge_tariff.send_to_unregistered
        total = amount + charge

        return self.account_balance >= total

class ChargeTariff(models.Model):
    """docstring for ChargeTariff."""
    min = models.IntegerField()
    max = models.IntegerField()
    withdraw_charge = models.IntegerField()
    send_to_unregistered = models.IntegerField()
    send_to_registered = models.IntegerField()
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

class MainTransaction(models.Model):
    """docstring for MainTransaction."""
    request_id = models.CharField(max_length=20)
    receipt = models.CharField(unique=True, max_length=20)
    partya = models.CharField(max_length=20)
    partyb = models.CharField(max_length=20)
    amount = models.DecimalField(max_digits=14, decimal_places=2)
    created_at = models.DateTimeField(auto_now_add=True)
    status = models.CharField(max_length=20)
    charge = models.IntegerField()

class SubTransaction(models.Model):
    """docstring for SubTransaction."""
    receipt = models.ForeignKey(MainTransaction, on_delete=models.CASCADE, to_field="receipt")
    amount = models.DecimalField(max_digits=14, decimal_places=2)
    amount_type = models.CharField(max_length=20)
    transaction_type = models.CharField(max_length=20)
    phone_number = models.CharField(max_length=20)
    created_at = models.DateTimeField(auto_now_add=True)

class BusinessAccount(models.Model):
    """docstring for BusinessAccount."""
    account_number = models.IntegerField()
    account_name = models.CharField(max_length=100)
    status = models.CharField(max_length=20)
    account_balance = models.DecimalField(max_digits=14, decimal_places=2)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
