from django.shortcuts import render
from rest_framework import viewsets

from paymentapi_python.paymentapi import models
from paymentapi_python.paymentapi import serializers

from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt

import json
import re
import random
import string

# Create your views here.

class ChargeTariffViewSet(viewsets.ModelViewSet):
    queryset = models.ChargeTariff.objects.all()
    serializer_class = serializers.ChargeTariffSerializer

class CustomerDetailViewSet(viewsets.ModelViewSet):
    queryset = models.CustomerDetail.objects.all()
    serializer_class = serializers.CustomerDetailSerializer

class MobileWalletViewSet(viewsets.ModelViewSet):
    queryset = models.MobileWallet.objects.all()
    serializer_class = serializers.MobileWalletSerializer

class BusinessAccountViewSet(viewsets.ModelViewSet):
    queryset = models.BusinessAccount.objects.all()
    serializer_class = serializers.BusinessAccountSerializer

class MainTransactionViewSet(viewsets.ModelViewSet):
    queryset = models.MainTransaction.objects.all()
    serializer_class = serializers.MainTransactionSerializer

class SubTransactionViewSet(viewsets.ModelViewSet):
    queryset = models.SubTransaction.objects.all()
    serializer_class = serializers.SubTransactionSerializer

@csrf_exempt
def c2ctransfer(request):
    try:
        response = {
            'success_code': 0,
            'success_message': ''
        }

        request_params = json.loads(request.body)
        required_params = {'partya', 'partyb', 'amount', 'pin'}

        if len(set(request_params.keys()).intersection(required_params)) < 4:
            response['success_code'] = 1
            response['success_message'] = 'required parameters: partya, partyb, amount, pin'
            return HttpResponse(json.dumps(response))

        partya = request_params['partya']
        partyb = request_params['partyb']
        amount = request_params['amount']
        pin = request_params['pin']

        phone_patterns = [r'^07[0-9]{8}$', r'^2547[0-9]{8}$', r'^7[0-9]{8}$']
        if True not in [bool(re.match(pattern, str(partya))) for pattern in phone_patterns]:
            response['success_code'] = 1
            response['success_message'] = 'party a phone format incorrect'
            return HttpResponse(json.dumps(response))

        if True not in [bool(re.match(pattern, str(partyb))) for pattern in phone_patterns]:
            response['success_code'] = 1
            response['success_message'] = 'party b phone format incorrect'
            return HttpResponse(json.dumps(response))

        partya = int(partya[-9:])
        partyb = int(partyb[-9:])
        amount = int(amount)

        if amount > 70000:
            response['success_code'] = 1
            response['success_message'] = 'That amount exceeds the limit allowed'
            return HttpResponse(json.dumps(response))

        mobile_wallet_a = models.MobileWallet.objects.get(pk=partya)
        mobile_wallet_b = models.MobileWallet.objects.get(pk=partyb)
        business_account = models.BusinessAccount.objects.first()

        if pin != mobile_wallet_a.pin:
            response['success_code'] = 1
            response['success_message'] = 'Incorrect PIN'
            return HttpResponse(json.dumps(response))

        charge_tariff = models.ChargeTariff.objects.filter(min__lte=amount, max__gte=amount)[0]
        to_registered = mobile_wallet_b.phone_number.status == 'registered'

        if not to_registered and amount > 30000:
            response['success_code'] = 1
            response['success_message'] = 'Amount must not exceed 30000 for unregistered accounts'
            return HttpResponse(json.dumps(response))

        if not mobile_wallet_a.has_enough_balance(amount,charge_tariff, to_registered):
            response['success_code'] = 1
            response['success_message'] = 'Insufficient balance'
            return HttpResponse(json.dumps(response))

        if mobile_wallet_b.phone_number.status == 'unregistered':
            charge_amount = charge_tariff.send_to_unregistered
        else:
            charge_amount = charge_tariff.send_to_registered

        total_charge = amount + charge_amount

        mobile_wallet_a.account_balance -= total_charge
        mobile_wallet_b.account_balance += amount
        business_account.account_balance += charge_amount

        mobile_wallet_a.save()
        mobile_wallet_b.save()
        business_account.save()

        receipt = ''.join(random.choices(string.ascii_uppercase + string.digits, k=10))

        while models.MainTransaction.objects.filter(receipt=receipt).count() > 0:
            receipt = ''.join(random.choices(string.ascii_uppercase + string.digits, k=10))
        
        main_transaction = models.MainTransaction()
        main_transaction.request_id = receipt
        main_transaction.receipt = receipt
        main_transaction.partya = str(partya)
        main_transaction.partyb = str(partyb)
        main_transaction.amount = amount
        main_transaction.charge = charge_amount
        main_transaction.status = 'success'
        main_transaction.save()

        sub_transaction_a = models.SubTransaction()
        sub_transaction_a.receipt = main_transaction
        sub_transaction_a.amount = amount
        sub_transaction_a.amount_type = 'transaction_amount'
        sub_transaction_a.transaction_type = 'debit'
        sub_transaction_a.phone_number = str(partya)
        sub_transaction_a.save()

        sub_transaction_b = models.SubTransaction()
        sub_transaction_b.receipt = main_transaction
        sub_transaction_b.amount = amount
        sub_transaction_b.amount_type = 'transaction_amount'
        sub_transaction_b.transaction_type = 'credit'
        sub_transaction_b.phone_number = str(partyb)
        sub_transaction_b.save()

        sub_transaction_c = models.SubTransaction()
        sub_transaction_c.receipt = main_transaction
        sub_transaction_c.amount = amount
        sub_transaction_c.amount_type = 'charge'
        sub_transaction_c.transaction_type = 'credit'
        sub_transaction_c.phone_number = business_account.account_name
        sub_transaction_c.save()

        response['receipt'] = receipt

        return HttpResponse(json.dumps(response))

    except json.decoder.JSONDecodeError:
        response['success_code'] = 1
        response['success_message'] = 'Incorrect JSON format'
        return HttpResponse(json.dumps(response))
